# Common DUECA policies


## Introduction

DUECA policies are an option implemented in the `dueca-gproject` project support script. Policies are checks on the coding and set-up of your project, and optional actions to modify your project. They are typically used to weed out "borrowing" of obsolete code, or to enforce specific coding standards.

This project assembles policy files, and makes them accessible for use by
dueca-gproject

## URL's and use

Given that this project is located at https://gitlab.tudelft.nl/dueca-ae-cs-policies/common , its main policy file can be downloaded 'raw' at

    https://gitlab.tudelft.nl/dueca-ae-cs-policies/common/-/raw/master/index.xml

This index file should contain the links to all active policy files in the repository.

When set in the environment variable

    DUECA_POLICIES=https://gitlab.tudelft.nl/dueca-ae-cs-policies/common/-/raw/master/index.xml

A run on the policies without exclusion of default policies, will fetch and apply these policies to your project:

    dueca-gproject policies

